# Webpack Yarn Scss - Barebones

A webpack development starter kit 

## Quick start (localhost)

1. Install node & yarn
2. Use correct node version ([nvm](https://github.com/nvm-sh/nvm), ``nvm use``)
3. ```cd assets/``` & ```yarn install```
4. ```yarn dev```
5. go to http://localhost:8080/


## Compile and use svgs
1. add svg files to ```/media/svg/folderName```
2. ```yarn compile```
3. ```yarn dev```
4. use ```{{ svg("name", "folder-name", "customClass", "color", "width", "a11y text") }}``` in the html
(``` example: {{ svg("facebook", "social-media", "null", "blue", "35px", "Follow us on facebook") }}```)


## Front-end Guidelines

- CSS Naming Convention: http://getbem.com/ (avoid utility based css)
- JS Use next generation JavaScript: https://babeljs.io/ (avoid jquery)

### CSS
- ```_config.scss``` to define colors
- ```_type.scss``` to define typography
- ```_fonts.scss``` to import the fonts
- ```_block.scss``` only contains the block ```.block```, it's elements ```.block__element``` and modifiers ```.block__element--modifier```
- no IDs for styling
- use ```.articles__item__image { @include object-fit(); }``` mixin instead of background image

### General
- use https://picsum.photos/600/500 for pictures
- no shortcuts. a button is a ```.button``` and not ```.btn```
- media queries are based on the foundation framework (```_menu.scss``` as example)

## Known caveats
- ```yarn dev``` Restart required after creating new file