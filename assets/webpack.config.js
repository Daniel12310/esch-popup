require('dotenv').config();
const glob = require("glob");
const globImporter = require('node-sass-glob-importer');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlReplaceWebpackPlugin = require('html-replace-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');
const postcssPresetEnv = require('postcss-preset-env');
const WebpackBuildNotifierPlugin = require('webpack-build-notifier');
const WebpackShellPlugin = require('webpack-shell-plugin-alt');
const SVGSpritemapPlugin = require('svg-spritemap-webpack-plugin');
const fs = require('fs');



/* Production/Development JS files
   Removes breakpoints.js from production
   Excludes gutenberg, admin, login from main.css to prevent duplication */
let mainJS = "./js/**/!(gutenberg|admin|login)*.js";
if (process.env.MODE === "production") {
    mainJS = "./js/**/!(breakpoints|gutenberg|admin|login)*.js";
}

/* WebpackBuildNotifierPlugin - Notification */
const notification = (process.env.DOMAIN) ? process.env.DOMAIN : "Compiled";

/* Source Map - Disable on production */
const sourceMap = (process.env.MODE != "production") ? "inline-source-map" : false;

module.exports = {
    mode: 'none',
    entry: {
        main: glob.sync(mainJS),
        // Create separate css files for
        gutenberg: "./js/dev/gutenberg.js",
        admin: "./js/dev/admin.js",
        login: "./js/dev/login.js",
    },
    performance: {
        hints: false
    },
    output: {
        pathinfo: false,
    },
    devtool: sourceMap,
    plugins: [
        new MiniCssExtractPlugin({
            filename: '[name].css'
        }),
        new WebpackBuildNotifierPlugin({
            title: notification,
            successSound: "Purr",
            failureSound: "Glass",
            suppressSuccess: true
        }),
        // create global spritemap for correct svg sizing
        new SVGSpritemapPlugin('media/svg/**/*.svg', {
            output: {
                filename: 'images/svg-library.svg',
            },
            sprite: {
                prefix: false,
            },
            styles: {
                filename: 'src/base/_icon-mixin.scss'
            }
        })
    ],
    module: {
        rules: [
            {
                test: /\.(png|jp(e*)g|gif)$/, // load all images
                loader: 'file-loader',
                include: path.resolve(__dirname, 'media'),
                options: {
                    name: 'images/[name].[ext]',
                },
            },
            {
                test: /\.(svg)$/, // load svgs in separate folder
                loader: 'file-loader',
                include: path.resolve(__dirname, 'media'),
                options: {
                    name: 'svg/[name].[ext]',
                },
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                loader: 'file-loader',
                include: path.resolve(__dirname, 'media'),
                options: {
                    name: 'fonts/[name].[ext]',
                },
            },
            {
                test: /\.scss$/,
                include: path.resolve(__dirname, 'src'),
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                        }
                    },
                    {
                        loader: "postcss-loader",
                        options: {
                            ident: 'postcss',
                            plugins: [
                                postcssPresetEnv(),
                            ],
                            sourceMap: true,
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true,
                            sassOptions: {
                                importer: globImporter()
                            }
                        }
                    }
                ],
            },
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                include: path.resolve(__dirname, 'js'),
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            }
        ],
    },
};

/* Web Dev Server - A local development server that provides live reloading. This should be used for development only.
   https://webpack.js.org/configuration/dev-server/
   Uses index.html with auto refresh on http://localhost:8080/
   Takes every .html file in the folder with index.html as the root */
if (process.env.DEV_SERVER) {
    let files = fs.readdirSync(path.resolve(__dirname, ''));
    const pages = files.filter((file) => file.endsWith('.html'));
    pages.forEach((page) => {
        let options = {
            template: page,
            meta: {
                viewport: "width=device-width, initial-scale=1.0"
            }
        }
        if (page !== 'index.html') {
            options.filename = page;
        }
        module.exports.plugins.push(
            new HtmlWebpackPlugin(options)
        );
        // replace svg twig function in dev server
        module.exports.plugins.push(
            new HtmlReplaceWebpackPlugin([
                {
                    // pattern: {{ svg("name", "folder", "customClass", "color", "width", "AriaLabel") }}
                    // pattern: {{ svg("facebook", "social-media", "null", "blue", "35px", "Follow us on facebook") }}
                    pattern: /{{ svg\("(.+?)"(?:, "(.+?)"(?:, "(.+?)")?(?:, "(.+?)")?(?:, "(.+?)")?(?:, "(.+?)")?)?\) }}/g,
                    replacement:
                        '<i class="icon icon--$1 $3" style="color:$4;width:$5;">\n' +
                        '<span class="a11y__vo-only">$6</span>\n' +
                        '<svg tabindex="-1" aria-hidden="true" focusable="false" ><use xlink:href="dist/images/$2.svg#$1"/></svg>\n' +
                        '</i>'
                }
            ])
        );
    })
}


/* SVG Spritesheet - Generates a single SVG spritemap containing multiple <symbol> elements from all .svg files in a directory.
   https://www.npmjs.com/package/svg-spritemap-webpack-plugin
   Creates spritemap based on the folder structure in svg */
let folders = fs.readdirSync(path.resolve(__dirname, 'media/svg'), { withFileTypes: true })
    .filter(dirent => dirent.isDirectory())
    .map(dirent => dirent.name);
folders.forEach((folder) => {
    module.exports.plugins.push(
        new SVGSpritemapPlugin(`media/svg/${folder}/*.svg`, {
            output: {
                filename: `images/${folder}.svg`,
                svg4everybody: true,
            },
            sprite: {
                prefix: false,
            }
        })
    );
});