export const apiUrl = () => {
    let elUpcoming = document.querySelector(".upcoming-events__container")?.getAttribute("data-api-url");
    let elAgenda   = document.querySelector(".agenda")?.getAttribute("data-api-url");

    /*if(elUpcoming) {
        return elUpcoming;
    } else if(elAgenda) {
        return elAgenda;
    }*/

    return "http://esch-popup-stores.docker.local/wp-json/api/v1/agenda-entries"
}

export const agendaPage = () => {
    let elUpcoming = document.querySelector(".upcoming-events__container")?.getAttribute("data-agenda-url");

    /*if(elUpcoming) {
        return elUpcoming;
    }*/

    return "http://localhost:8080/agenda.html"
}


export default { apiUrl, agendaPage }