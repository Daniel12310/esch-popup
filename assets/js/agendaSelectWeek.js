import regeneratorRuntime from "regenerator-runtime"; // Is needed for async/await
import moment from "moment";

/*** Add EventListener to Calendar to select by week ***/
const addWeekSelectEvent = (calendar_el, calendarPicker, fetch = null) => {

    calendar_el?.addEventListener('calendar-select', (ev) => {
        const date = calendarPicker.getSelected();
        toggleItems(date);
        fetch(moment(date, "DD-MM-YYYY"));
    })

    const toggleItems = (day) => {
        //console.log(day);
        let week = moment(day, "DD-MM-YYYY").week();
        let year = moment(day, "DD-MM-YYYY").year();
        let count = 0;

        document.querySelectorAll(".event__item")?.forEach(item => {
            let cnFound     = false;
            let className   = "";
            let i           = 0;

            while(!cnFound || i<item.classList.length) {
                if(item.classList[i].startsWith("js-event-date")) {
                    className = item.classList[i];
                    cnFound = true;
                }
                i++;
            }

            let dates       = className.split("__")[1].split("_");
            let fromWeek    = dates[0].split("-")[0];
            let fromYear    = dates[0].split("-")[1];
            let toWeek      = dates[1].split("-")[0];
            let toYear      = dates[1].split("-")[1];

            item.classList.remove("event__item--hidden");
            let isYear = year>=fromYear && year<=toYear;
            let isWeek = week>=fromWeek && week<=toWeek;
            //("isYear: " + isYear + "; isWeek: " + isWeek);
            if(!isYear || !isWeek) {
                item.classList.add("event__item--hidden");
            } else {
                count++;
            }
        });

        document.querySelector(".agenda__results__number").textContent = count;

    };

}

export default addWeekSelectEvent;