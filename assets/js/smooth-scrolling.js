document.addEventListener('click', (ev) => {
    if (ev.target.classList.contains('js-scroll')) {
        ev.preventDefault();
        scrollTo(ev.target);
    }
});

// use
// scrollTo('#main');
// scrollTo(object);  -> object with href or data-location
// <a href="#main" class="js-scroll">Scroll to main</a>
// <button data-location="#main" class="js-scroll">Scroll to main</button>
export function scrollTo(scroll) {
    let scrollId;
    if (typeof scroll === 'string' || scroll instanceof String) {
        scrollId = scroll;
    } else if (scroll.dataset.location) {
        scrollId = scroll.dataset.location;
    } else {
        scrollId = scroll.getAttribute('href');
    }
    //prefix string with # if not present
    scrollId = (scrollId.charAt(0) !== '#') ? ("#" + scrollId) : scrollId;
    const scrollToSection = document.querySelector(scrollId);
    if (scrollToSection) {
        const distanceToTop =
            window.pageYOffset
            + scrollToSection.getBoundingClientRect().top;

        scrollToCallback(parseInt(distanceToTop), () => {
            scrollToSection.focus({preventScroll:true});
            if(document.activeElement !== scrollToSection) {
                scrollToSection.setAttribute('tabindex', '-1');
                scrollToSection.focus({preventScroll:true});
            }
        });
    }
}

//taken from https://stackoverflow.com/a/55686711
/**
 * Native scrollTo with callback
 * @param offset - offset to scroll to
 * @param callback - callback function
 */
function scrollToCallback(offset, callback) {
    const onScroll = function () {
        if (window.pageYOffset === offset) {
            window.removeEventListener('scroll', onScroll);
            callback()
        }
    };
    window.addEventListener('scroll', onScroll);
    onScroll();
    window.scrollTo({
        top: offset,
        behavior: 'smooth'
    })
}
