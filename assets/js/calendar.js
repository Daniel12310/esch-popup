import regeneratorRuntime from "regenerator-runtime"; // Is needed for async/await
import moment from "moment";
import Flickity from 'flickity-imagesloaded';
import TavoCalendar from "./tavo";
import axios from "axios";
import categoriesAddToggleEvent from './agendaCategories';
import addWeekSelectEvent from './agendaSelectWeek';
import agendaUpcomingWeeks from './agendaUpcomingWeeks';
import { apiUrl, agendaPage } from './urls';

let calendar_el;
let calendarPicker;

moment.updateLocale('fr', {
    week: {
        dow : 1,
    }
});
let isInitalCall = true;

const calendar = () => {
    let currentPagination = 1;
    let eventsCount = 0;
    let currentDate = moment();

    const initCalendar = () => {
        calendar_el = document.querySelector('#js-calendar-picker');
        let date = currentDate;
        if(calendar_el) {
            calendarPicker = new TavoCalendar(calendar_el, {
                //range_select: true
                date: date,
                selected: [date],
            });
        }
        calendar_el?.addEventListener('calendar-range', (ev) => {
            const range = calendarPicker.getRange();
        });
    }

    const clearDomContent = () => {
        let evContainer = document.querySelector(".js-events-container")
        let evUpcoming = document.querySelector(".js-upcoming-events-container")
        if(evContainer) evContainer.innerHTML = "";
        if(evUpcoming) evUpcoming.innerHTML = "";
        //if(!isInitalCall) setTimeout(() => {console.log("timout")}, 1000)
    }

    const fetchEvents = async (date) => {
        clearDomContent();
        createLoadingSpinner();

        let events = [];
        let container = document.querySelector(".js-events-container");
        let upcoming_container = document.querySelector(".js-upcoming-events-container")

        //console.log("FetchEvents date: " + date);
        //console.log(apiUrl() + `?date=${moment(currentDate).format("YYYY-MM-DD")}`);
        if(date) {
            currentDate = moment(date);
        }

        //console.log(moment(currentDate).format("YYYY-MM-DD"));
        console.log(apiUrl() + `?date=${currentDate.format("YYYY-MM-DD")}`);

        if (container !== null || upcoming_container !== null) {
            await axios
                .get(apiUrl() + `?date=${currentDate.format("YYYY-MM-DD")}`) // + `?page=${currentPagination}`)
                .then(result => {
                    events = result.data;
                    eventsCount+=events.length;
                    console.log(events);
                }).catch(err => {
                    console.log("Event fetch error: ", err);
                });
            await createDomEvents(events);

            createFlickityHome();

            isInitalCall = false;
            return events;
        }
        return [];
    }

    const createDomEvents = async (events) => {
        /** Select Container **/
        let container = document.querySelector(".js-events-container");
        let upcoming_container = document.querySelector(".js-upcoming-events-container");
        let upcoming = document.querySelector(".js-upcoming-events-container .flickity-slider");

        /** If events is empty, show message **/
        if(!events || events.length === 0) {
            let emptyMessage = document.createElement("h3");
            emptyMessage.textContent = "Aucun événement pour la semaine sélectionné."
            container?.appendChild( emptyMessage);
            upcoming_container?.appendChild(emptyMessage)
        }

        /** Iterate through every event and create the DOM-Elements **/
        await events?.forEach((event, i) => {
            /** Check if the event is published **/
            if (event.post_status === "publish") {

                /** Create event__item with classes **/
                let item = document.createElement("div");
                item.classList.add("event__item");
                if (event.field_agenda_entry_category === "Services")
                    item.classList.add("event-category__services");
                else if (event.field_agenda_entry_category === "Food")
                    item.classList.add("event-category__food");
                else if (event.field_agenda_entry_category === "Shop")
                    item.classList.add("event-category__shop");
                else if (event.field_agenda_entry_category === "Expo")
                    item.classList.add("event-category__event");

                if (event.field_agenda_entry_action_url_is_social === true)
                    item.classList.add("event-action__social");
                else if (event.field_agenda_entry_action_url !== "")
                    item.classList.add("event-action__website");
                else if (event.field_agenda_entry_action_email !== "")
                    item.classList.add("event-action__mail");



                /** Create category_button **/
                let button = document.createElement("div");
                button.classList.add("event__item__category_button");
                item.appendChild(button);

                /** Create event__item__header **/
                let header = document.createElement("header");
                header.classList.add("event__item__header", "event-category__shop");
                item.appendChild(header);

                /** Create item__image inside Header **/
                let image = document.createElement("img");
                image.classList.add("event__item__image");
                //if (event.field_agenda_entry_image_url !== "")
                image.src = event.field_agenda_entry_image_url;
                //else
                // image.src = "https://picsum.photos/600/500";
                header.appendChild(image);

                /** Create item__content **/
                let content = document.createElement("div");
                content.classList.add("event__item__content");
                item.appendChild(content);

                /** Create heading/title inside Content **/
                let title = document.createElement("h3");
                title.classList.add("event__item__title");
                title.textContent = event.field_agenda_entry_title;
                content.appendChild(title);

                /** Create date/time inside Content **/
                let time = document.createElement("time");
                //console.log(moment(event.field_agenda_entry_pop_up_from.split(' ')[0]).week());
                time.classList.add("event__item__date");
                let fromArray = event.field_agenda_entry_pop_up_from.split(' ')[0];
                let toArray = event.field_agenda_entry_pop_up_until.split(' ')[0];
                item.classList.add(`js-event-date__${moment(fromArray).week()}-${moment(fromArray).year()}_${moment(toArray).week()}-${moment(toArray).year()}`);
                let from = fromArray.split('-')[2] + "/" + fromArray.split('-')[1];
                let to = toArray.split('-')[2] + "/" + toArray.split('-')[1] + "/" + toArray.split('-')[0];
                time.textContent = `Du ${from} au ${to}`;
                content.appendChild(time);

                if(upcoming_container) {
                    let isYear = moment().year()>=moment(fromArray).year() && moment().year()<=moment(toArray).year();
                    let isWeek = moment().week()>=moment(fromArray).week() && moment().week()<=moment(toArray).week();

                    if(!isYear || !isWeek) {
                        //item.classList.add("event__item--hidden");
                    }
                }

                /** Create description inside Content **/
                let desc = document.createElement("p");
                desc.classList.add("event__item__desc");
                desc.innerHTML = event.field_agenda_entry_description;
                //desc.innerHTML = event.field_agenda_entry_description.length > 100 ? event.field_agenda_entry_description.substring(0,100) + "..." : event.field_agenda_entry_description; //"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci aliquam aperiam atque aut deleniti distinctio dolor dolorem dolorum, est hic impedit ipsa iure maxime, minus necessitatibus neque, pariatur. Laboriosam, molestiae!";
                content.appendChild(desc);

                /** Create link **/
                let link = document.createElement("a");
                if (event.field_agenda_entry_action_url_is_social === true)
                    link.href = event.field_agenda_entry_action_url;
                else if (event.field_agenda_entry_action_url !== "")
                    link.href = event.field_agenda_entry_action_url;
                else if (event.field_agenda_entry_action_email !== "")
                    link.href = `mailto:${event.field_agenda_entry_action_email}`;
                item.appendChild(link);

                /** Create item__action-button **/
                let action_button = document.createElement("div");
                action_button.classList.add("event__item__action-button");
                link.appendChild(action_button);

                /** Append item to dom **/
                container?.appendChild(item);
                upcoming_container?.appendChild(item);
            }

            categoriesAddToggleEvent();

        });

        upcoming_container?.appendChild(createShowFullCalendar());

        const loadingSpinner = document.querySelector(".loading-spinner");
        if(loadingSpinner) {
            container?.removeChild(loadingSpinner);
            upcoming_container?.removeChild(loadingSpinner);
        }
        /** Set Results-length **/
        let results_number = document.querySelector(".agenda__results__number");
        if (results_number !== null)
            results_number.textContent = events?.length;

    }

    const createShowFullCalendar = () => {
        /** Create ShowFullCalendar for upcomping_container **/
        let showFullCalendar = document.createElement("a");
        showFullCalendar.href = agendaPage();

        let showFullCal_item = document.createElement("div");
        showFullCal_item.classList.add("event__item__show-all");

        let showFullCal_heading = document.createElement("h2");
        showFullCal_heading.classList.add("h_color__white");
        showFullCal_heading.textContent = "Voir le calendrier complet";

        showFullCal_item.appendChild(showFullCal_heading);
        showFullCalendar.appendChild(showFullCal_item);

        return showFullCalendar;
    }

    const createLoadingSpinner = () => {
        let loading = document.createElement("div");
        loading.classList.add("loading-spinner");
        loading.innerHTML = "<p>Loading...</p>";
        document.querySelector(".js-events-container")?.append(loading);
        document.querySelector(".js-upcoming-events-container")?.append(loading);
        initCalendar();
    }

    const createFlickityHome = () => {
        let flickityDomItem = document.querySelector(".js-agenda-carousel");
        if(flickityDomItem) {
            let flickity = new Flickity(flickityDomItem, {
                pageDots: false,
                contain: true,
                imagesLoaded: true,
                cellAlign: 'left',
            });
        }

    }

    const reloadFlickityHome = () => {
        //let flickityDomItem = document.querySelector(".js-agenda-carousel");
        let flick = Flickity.data(".js-agenda-carousel");
        flick?.destroy();
        console.log("Reload flickity home")
    }

    const createDomSeeMore = () => {
        const seeMoreButton = document.createElement("button");
        seeMoreButton.classList.add("agenda__see-more__button");
        seeMoreButton.textContent = "Voir plus";
        if(eventsCount<6) {
            seeMoreButton.classList.add("see-more--hidden");
        }

        seeMoreButton.addEventListener("click", async (ev) => {
            ev.preventDefault();
            currentPagination++;
            let events = await fetchEvents();
            await createDomEvents(events);
        });

        document.querySelector(".agenda__see-more")?.appendChild(seeMoreButton);
    }


    const init = async () => {
        //createLoadingSpinner();
        let events = await fetchEvents();
        createFlickityHome();
        addWeekSelectEvent(calendar_el, calendarPicker, fetchEvents);
        agendaUpcomingWeeks(fetchEvents);
        //createDomSeeMore();
    }

    init().catch(err => console.error("Init calendar throws error: ", err));
}
document.addEventListener("DOMContentLoaded", async function(event) {
    calendar();
});

