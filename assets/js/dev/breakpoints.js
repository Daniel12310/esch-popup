const breakpoints = document.createElement('span');
breakpoints.classList.add('js-breakpoint');
breakpoints.style.cssText = 'pointer-events: none;position:fixed;right:0;top:25px;background-color:rgba(255, 255, 255, 0.8);z-index:999999;padding: 5px 10px;';
document.body.appendChild(breakpoints);

function calcBreakpoint() {
    let breakpoint;
    if (window.innerWidth < 480) {
        breakpoint = 'small';
    } else if (window.innerWidth < 720) {
        breakpoint = 'medium';
    } else if (window.innerWidth < 1024) {
        breakpoint = 'xmedium';
    } else if (window.innerWidth < 1200) {
        breakpoint = 'large';
    } else if (window.innerWidth < 1570) {
        breakpoint = 'xlarge';
    } else if (window.innerWidth < 2000) {
        breakpoint = 'xxlarge';
    } else {
        breakpoint = 'cap';
    }
    document.querySelector('.js-breakpoint').innerText = breakpoint;
}

calcBreakpoint();
window.addEventListener('resize', calcBreakpoint);