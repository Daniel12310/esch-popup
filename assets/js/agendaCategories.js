/*** Agenda Category-selector to toggle showed categories ***/
const addToggleEvent = () => {
    const agendaItems = document.querySelectorAll(".event__item");

    /** Click "All"-button **/
    document
        .querySelector(".js-category-toggle__all")?.addEventListener("click",
        () => {
            toggleItems("all");
        });

    /** Click "Shop"-button **/
    document
        .querySelector(".js-category-toggle__shop")?.addEventListener("click",
        () => {
            toggleItems("event-category__shop");
        });

    /** Click "Food"-button **/
    document
        .querySelector(".js-category-toggle__food")?.addEventListener("click",
        () => {
            toggleItems("event-category__food");
        });

    /** Click "Services"-button **/
    document
        .querySelector(".js-category-toggle__services")?.addEventListener("click",
        () => {
            toggleItems("event-category__services");
        });

    /** Click "Event"-button **/
    document
        .querySelector(".js-category-toggle__event")?.addEventListener("click",
        () => {
            toggleItems("event-category__event");
        });


    const toggleItems = (category) => {
        if (category === "all") {
            agendaItems?.forEach(item => {
                item.classList.remove("event__item--hidden");
            });
            document.querySelector(".agenda__results__number").textContent = agendaItems?.length;
        } else {
            let count = 0;
            agendaItems?.forEach(item => {
                item.classList.remove("event__item--hidden");
                if (!item.classList.contains(category)) {
                    item.classList.add("event__item--hidden");
                } else {
                    count++;
                }
            });
            document.querySelector(".agenda__results__number").textContent = count;
        }
    };
}
export default addToggleEvent;