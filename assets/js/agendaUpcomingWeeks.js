/*<div class="upcoming-events__week upcoming-events__week--active">
    <span class="upe__week-day">20</span>
    <span class="upe__week-month">Juillet</span>
    <span class="upe__week-results">10 résults</span>
</div>*/

import regeneratorRuntime from "regenerator-runtime"; // Is needed for async/await
import moment from "moment";
import Flickity from 'flickity-imagesloaded';
import { apiUrl, agendaPage } from './urls';

moment.updateLocale('fr', {
    week: {
        dow : 1,
    }
});

/*** Add EventListener to Calendar to select by week ***/
const agendaUpcomingWeeks = (fetch = null) => {
    const container = document.querySelector(".upcoming-events__weeks-container");

    const getWeeks = () => {
        let today = moment().weekday(0);

        let weeks = [today];
        for(let i = 1; i<=5; i++) {
            weeks.push(today.clone().add(i, 'weeks'));
        }
        return weeks;
    }

    const createDomElements = () => {
        moment.locale('fr');
        const weeks = getWeeks();

        weeks?.forEach((week, index) => {
            let dateClass = `js-upcoming-week__${moment(week).week()}-${moment(week).year()}`

            /** Create upcoming-events__week container **/
            let item = document.createElement("div");
            item.classList.add("upcoming-events__week", dateClass);
            if(index===0) {
                item.classList.add("upcoming-events__week--active");
            }

            let showDay = document.createElement("div");
            showDay.classList.add("upe__week-day");
            showDay.textContent = moment(week).date();
            item.appendChild(showDay);

            let showMonth = document.createElement("div");
            showMonth.classList.add("upe__week-month");
            showMonth.textContent = moment(week).format('MMMM');
            item.appendChild(showMonth);

            let showResults = document.createElement("div");
            showResults.classList.add("upe__week-results");
            let count = getResultsPerWeek(moment(week).week(), moment(week).year());
            showResults.textContent = `${count} résultats`;
            item.appendChild(showResults);

            container?.appendChild(item);
        });

        /*<div class="upcoming-events__week__full-calendar">
            {{ svg("agenda_complet", "ui", "null", "white", "42px") }}
        </div>*/
        let fullCalendarLink = document.createElement("a");

        fullCalendarLink.href = agendaPage();

        let fullCalendar = document.createElement("div");
        fullCalendar.classList.add("upcoming-events__week__full-calendar");
        fullCalendarLink.appendChild(fullCalendar)
        container?.appendChild(fullCalendarLink);
    }

    const addEventListeners = () => {
        const weekItems = document.querySelectorAll(".upcoming-events__week");
        weekItems?.forEach(item => {
            item.addEventListener("click", () => {
                let className = getJsClass(item, "js-upcoming-week");

                let cnDate = className.split("__")[1].split("-");
                let week = cnDate[0];
                let year = cnDate[1];

                weekItems.forEach((weekitem) => {
                    weekitem.classList.remove("upcoming-events__week--active");
                });
                item.classList.add("upcoming-events__week--active");


                fetch(moment().year(year).week(week));
                //toggleItems(week, year);

            })
        });
    }

    const getResultsPerWeek = (week, year) => {
        const events = document.querySelectorAll(".event__item");
        let count = 0;
        events?.forEach(item => {
            let className = getJsClass(item, "js-event-date");

            let dates       = className.split("__")[1].split("_");
            let fromWeek    = dates[0].split("-")[0];
            let fromYear    = dates[0].split("-")[1];
            let toWeek      = dates[1].split("-")[0];
            let toYear      = dates[1].split("-")[1];


            let isYear = year>=fromYear && year<=toYear;
            let isWeek = week>=fromWeek && week<=toWeek;
            //console.log("isYear: " + isYear + "; isWeek: " + isWeek);
            if(isYear && isWeek) {
                count++;
            }
        });
        return count;
    }

    const getJsClass = (item, jsClass) => {
        let cnFound = false;
        let className = "";
        let i = 0;

        while (!cnFound || i < item.classList.length) {
            if (item.classList[i].startsWith(jsClass)) {
                className = item.classList[i];
                cnFound = true;
            }
            i++;
        }

        return className;
    }

    const toggleItems = (week, year) => {
        document.querySelectorAll(".event__item")?.forEach(item => {
            let className = getJsClass(item, "js-event-date");

            let dates       = className.split("__")[1].split("_");
            let fromWeek    = dates[0].split("-")[0];
            let fromYear    = dates[0].split("-")[1];
            let toWeek      = dates[1].split("-")[0];
            let toYear      = dates[1].split("-")[1];

            item.classList.remove("event__item--hidden");
            let isYear = year>=fromYear && year<=toYear;
            let isWeek = week>=fromWeek && week<=toWeek;
            //console.log("isYear: " + isYear + "; isWeek: " + isWeek);
            if(!isYear || !isWeek) {
                item.classList.add("event__item--hidden");
            }
        });

        let flickity = document.querySelector(".js-agenda-carousel");
        if(flickity)
            Flickity.data(flickity).reloadCells();
    };

    const makeFlickityForWeeks = () => {
        let el = document.querySelector(".upcoming-events__weeks-container");
        if(el) {
            let flickity = new Flickity(".upcoming-events__weeks-container", {
                pageDots: false,
                prevNextButtons: false,
                cellAlign: 'left',
                contain: true,
                imagesLoaded: true,
            });
        }
    }

    const init = async () => {
        await createDomElements();
        addEventListeners();
        makeFlickityForWeeks();
    }

    init();


}

export default agendaUpcomingWeeks;

