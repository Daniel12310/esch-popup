import { disableScroll, enableScroll } from './helpers';
import { trapFocus } from './helpers';

document.addEventListener('click', function (ev) {
    if (ev.target.classList.contains('js-menu__toggle-button')) {
        toggleMenu();
    }
});

function toggleMenu() {
    const menu = document.querySelector('.js-menu');
    if (menu.classList.contains('is-active')) {
        menu.classList.remove('is-active');
        enableScroll();
    } else {
        menu.classList.add('is-active');
        disableScroll();
        trapFocus(menu);
    }
}