import objectFitImages from 'object-fit-images';
import smoothscroll from 'smoothscroll-polyfill';
import 'focus-within-polyfill';
import svg4everybody from 'svg4everybody';

smoothscroll.polyfill();
objectFitImages();
svg4everybody();

// missing forEach on NodeList for IE11
// https://github.com/miguelcobain/ember-paper/issues/1058#issuecomment-461764542
if (window.NodeList && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = Array.prototype.forEach;
}